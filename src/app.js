import React from 'react';
import {
  Text,
  View,
  TouchableHighlight,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Button,
  FlatList,
  ImageBackground,
  Image,
  Dimensions,
  KeyboardAvoidingView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {Data} from '../../Data/Data';
import {ADD_TO_CART, REMOVE_FROM_CART} from '../../Redux/Action/CartItems';
import {useDispatch, useSelector} from 'react-redux';
const {width, height} = Dimensions.get('window');

const Book = (props) => {
  const dispatch = useDispatch();
  const addItemToCart = (item) => dispatch({type: ADD_TO_CART, payload: item});
  const removeItemFromCart = (item) =>
    dispatch({type: REMOVE_FROM_CART, payload: item});

  const cartItems = useSelector((state) => state);

  const renderItem = ({item, index}) => {
    return (
      <View
        style={{
          width: '94%',
          height: width / 2.5,
          margin: (width * 0.2) / 12,
          backgroundColor: '#d3d3d3',
          flexDirection: 'row',
          padding: 10,
          borderRadius: 10,
          alignSelf: 'center',
          justifyContent: 'center',
        }}>
        <Image source={{uri: item.imgUrl}} style={styles.thumbnail} />
        <View
          style={{
            paddingHorizontal: 10,
            marginHorizontal: 10,
            width: width / 1.5,
            // backgroundColor: 'red',
            justifyContent: 'space-around',
          }}>
          <Text>
            {' '}
            <Text style={{color: '#000', fontWeight: 'bold'}}>Name: </Text>{' '}
            {item.name}
          </Text>
          <Text>
            {' '}
            <Text style={{color: '#000', fontWeight: 'bold'}}>
              auther Name:{' '}
            </Text>{' '}
            {item.author}
          </Text>
          <Text numberOfLines={2}>
            {' '}
            <Text style={{color: '#000', fontWeight: 'bold'}}>
              Description:{' '}
            </Text>{' '}
            {item.description}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              width: width * 0.4,
              height: (width * 0.2) / 3,
              // backgroundColor: '#fff',
              alignItems: 'center',
              // borderRadius: 10,
            }}>
            <TouchableOpacity
              onPress={() => removeItemFromCart(item)}
              style={{
                backgroundColor: 'red',
                width: (width * 0.2) / 1.2,
                height: (width * 0.2) / 2.5,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
              }}>
              <Text style={{color: '#fff', fontWeight: 'bold'}}>Remove</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => addItemToCart(item)}
              style={{
                backgroundColor: 'blue',
                width: (width * 0.2) / 1.2,
                height: (width * 0.2) / 2.5,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
              }}>
              <Text style={{color: '#fff', fontWeight: 'bold'}}>Add</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };
  return (
    // <KeyboardAvoidingView
    //   behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
    //   style={styles.container}>
    <SafeAreaView style={styles.conatainer}>
      <View
        style={{
          backgroundColor: '#d3d3d3',
          width: width,
          height: (width * 0.2) / 1.5,
          justifyContent: 'space-between',
          alignItems: 'center',
          flexDirection: 'row',
          paddingHorizontal: 10,
        }}>
        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <Icon name="chevron-back" size={(width * 0.2) / 2.5} />
        </TouchableOpacity>
        <Text style={{fontSize: (width * 0.2) / 5}}>Books Screen</Text>
        <TouchableOpacity onPress={() => props.navigation.navigate('Cart')}>
          <Icon name="cart" size={(width * 0.2) / 2.5} />
          <View style={styles.itemCountContainer}>
            <Text style={styles.itemCountText}>{cartItems.length}</Text>
          </View>
        </TouchableOpacity>
      </View>
      <FlatList
        data={Data}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </SafeAreaView>
    // {/* </KeyboardAvoidingView> */}
  );
};

export default Book;
const styles = StyleSheet.create({
  conatainer: {
    flex: 1,
  },
  image: {flex: 1, resizeMode: 'cover', justifyContent: 'center'},
  text: {
    fontSize: 24,
    left: '30%',
    justifyContent: 'center',
    alignSelf: 'center',
    fontWeight: '600',
    color: '#FFFF',
  },
  bookItemContainer: {
    flexDirection: 'row',
    padding: 10,
  },
  thumbnail: {
    width: width * 0.2,
    height: width * 0.3,
  },
  bookItemMetaContainer: {
    padding: 5,
    paddingLeft: 10,
  },
  textTitle: {
    fontSize: 22,
    fontWeight: '400',
    color: '#fff',
  },
  textAuthor: {
    fontSize: 18,
    fontWeight: '200',
    color: '#fff',
  },
  buttonContainer: {
    position: 'absolute',
    top: 110,
    left: 10,
  },
  button: {
    borderRadius: 8,
    backgroundColor: '#24a0ed',
    padding: 5,
  },
  buttonText: {
    fontSize: 22,
    color: '#fff',
  },
  button2: {
    marginRight: '10%',
  },
  itemCountContainer: {
    position: 'absolute',
    height: 20,
    width: 20,
    borderRadius: 15,
    backgroundColor: '#24a0ed',
    right: 20,
    bottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemCountText: {
    color: 'white',
    fontWeight: 'bold',
  },
});
